import { Component } from '@angular/core';
import { Event as NavigationEvent } from '@angular/router';
import { filter } from 'rxjs/operators';
import { NavigationStart } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'nhmortgage';


}
