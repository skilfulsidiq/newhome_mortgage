import { ApplicationComponent } from './views/forms/application/application.component';
import { AffordabilityComponent } from './views/forms/affordability/affordability.component';
import { HomeloanComponent } from './views/homeloan/homeloan.component';
import { NotfoundComponent } from './views/notfound/notfound.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './views/welcome/welcome.component';
import { SelectloanComponent } from './views/selectloan/selectloan.component';
import { NhfComponent } from './views/nhf/nhf.component';
import { EligibilityComponent } from './views/forms/eligibility/eligibility.component';

const routes: Routes = [
  { path: '', redirectTo: 'welcome', pathMatch:  'full' },
  { path: 'welcome',  component: WelcomeComponent},
  { path: 'select', component: SelectloanComponent },
  { path: 'homeloan', component: HomeloanComponent },
  { path: 'nhf', component: NhfComponent},
  { path: 'eligibility', component: EligibilityComponent },
  { path: 'affordability', component: AffordabilityComponent },
  { path: 'nhfapplication', component: ApplicationComponent },
  { path: '**', component: WelcomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
