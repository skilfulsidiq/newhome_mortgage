import { Angular4PaystackModule } from 'angular4-paystack';
import { ApiService } from './services/api.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ReactiveFormsModule } from '@angular/forms';

import { FooterComponent } from './template/footer/footer.component';
import { WelcomeComponent } from './views/welcome/welcome.component';
import { NotfoundComponent } from './views/notfound/notfound.component';
import { SelectloanComponent } from './views/selectloan/selectloan.component';
import { NhfComponent } from './views/nhf/nhf.component';
import { EligibilityComponent } from './views/forms/eligibility/eligibility.component';
import { AffordabilityComponent } from './views/forms/affordability/affordability.component';
import { ApplicationComponent } from './views/forms/application/application.component';
import { HomeloanComponent } from './views/homeloan/homeloan.component';
import { TopComponent } from './template/top/top.component';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    WelcomeComponent,
    NotfoundComponent,
    SelectloanComponent,
    NhfComponent,
    EligibilityComponent,
    AffordabilityComponent,
    ApplicationComponent,
    HomeloanComponent,
    TopComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Angular4PaystackModule,
    ReactiveFormsModule,
    HttpClientModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
