import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NhfComponent } from './nhf.component';

describe('NhfComponent', () => {
  let component: NhfComponent;
  let fixture: ComponentFixture<NhfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NhfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NhfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
