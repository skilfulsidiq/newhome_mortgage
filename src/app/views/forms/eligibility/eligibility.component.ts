import { ApiService } from './../../../services/api.service';

import { Component, OnInit, AfterViewInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';


@Component({
  selector: 'app-eligibility',
  templateUrl: './eligibility.component.html',
  styleUrls: ['./eligibility.component.css']
})
export class EligibilityComponent implements AfterViewInit {
  currentTab = 0;
  showElement = false;
  isSubmitted = false;
  btnText = 'Next';
  isvalid = true;
  isloading = false;
  form = new FormGroup({
    'is_nhf_contributor':       new FormControl('', Validators.required),
  'reg_number':                 new FormControl('', Validators.required),
    'six_month_contribution':   new FormControl('', Validators.required),
    'found_property':           new FormControl('', Validators.required),
    'yob':                      new FormControl('', Validators.required),
    'name':                     new FormControl('', Validators.required),
    'email':                    new FormControl('', Validators.required),
  });
  constructor(public api:  ApiService) { 
    // console.log(this.form.get('is_nhf_contributor').touched);
  }

  ngAfterViewInit() {
    this.showTab(this.currentTab);
  }

  showTab(n: number) {
    const tabElements = document.getElementsByClassName('tab');
    const tabToDisplay = tabElements.item(n) as HTMLElement;
    tabToDisplay.style.display = 'block';
    if (n === 0) {
      this.showElement = false;
    } else {
      this.showElement = true;
    }
    if (n === (tabElements.length - 1)) {
      this.btnText = 'Submit';
    } else {
      this.btnText = 'Next';
    }
    this.fixStepIndicator(n);
  }
  prevStep(n) {
    const elem = document.getElementsByClassName('tab');
    const x = elem.item(this.currentTab) as HTMLElement;
    x.style.display = 'none';
    // decrease the current tab by 1:
    this.currentTab -= n;
    this.showTab(this.currentTab);
  }
  nextStep(n) {
    const elem = document.getElementsByClassName('tab');
    const x = elem.item(this.currentTab) as HTMLElement;
    if (n == 1 && !this.validateForm()) {
      return false;
    }
    // console.log('ok');
      x.style.display = 'none';
    // Increase the current tab by 1:
      this.currentTab = this.currentTab + n;
      this.isvalid = true;
      console.log(this.currentTab);
    // if you have reached the end of the form... :
    if (this.currentTab >= elem.length) {
      // document.getElementById("regForm").submit();
      this.submitEligibilityForm();
      return false;
    }
  this.showTab(this.currentTab);
  }
  fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    const elem = document.getElementsByClassName('step');
    // const x = elem.item(n) as HTMLElement;
    for (let i = 0; i < elem.length; i++) {
      elem[i].className = elem[i].className.replace(' active', '');
    }
    elem[n].className += ' active';
  }
  validateForm() {
  // This function deals with validation of the form fields
  let valid2 = true;
  const x = document.getElementsByClassName('tab');
  const y = x.item(this.currentTab) as HTMLElement;
  const z = y.getElementsByTagName('input');

  // A loop that checks every input field in the current tab:
  for (let i = 0; i < z.length; i++) {
    // console.log(z[i].name);
    if (!this.form.get(z[i].name).touched && this.form.get(z[i].name).invalid ) {
      // add an "invalid" class to the field:
        this.isvalid = false;
      // and set the current valid status to false:
        valid2 = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid2) {
    document.getElementsByClassName('step')[this.currentTab].className += ' finish';
  }
  return valid2; // return the valid status
}

  submitEligibilityForm() {
    // this.isSubmitted = true;
    this.isloading = true;
    if  (this.form.valid) {
      console.log(this.form.value);
        this.api.submitEligibilty(this.form.value)
        .subscribe(res  =>  {
          this.form.reset();
          this.isloading = false;
          console.log(res);
        },
        err =>  {
          console.log(err);
          this.isloading = false;
        });
    }
  }

}
