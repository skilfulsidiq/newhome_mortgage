import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectloanComponent } from './selectloan.component';

describe('SelectloanComponent', () => {
  let component: SelectloanComponent;
  let fixture: ComponentFixture<SelectloanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectloanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectloanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
