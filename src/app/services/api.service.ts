import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const headers = new HttpHeaders();
headers.append('Access-Control-Allow-Origin', '*');
headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
headers.append('Accept', 'application/json');
headers.append('content-type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private url = 'http://newhomesdashboard.test/api/';
  constructor(public http: HttpClient) {
    console.log('Hello AuthProvider Provider');
  }


  // makeFavourite(credential) {
  //   console.log(credential);
  //   return this.http.post(authUrl + 'makeFavourite', credential, { headers: headers });
  // }

  submitEligibilty(credential)  {
    console.log(credential);
    return this.http.post(this.url +  'mortgage/eligibility',  credential, {headers: headers});
  }

}
